package me.jiaxiu.mapper;

import me.jiaxiu.my.mapper.MyMapper;
import me.jiaxiu.pojo.Fans;
import org.springframework.stereotype.Repository;

@Repository
public interface FansMapper extends MyMapper<Fans> {
}