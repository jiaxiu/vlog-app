package me.jiaxiu.mapper;

import me.jiaxiu.my.mapper.MyMapper;
import me.jiaxiu.pojo.Comment;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentMapper extends MyMapper<Comment> {
}