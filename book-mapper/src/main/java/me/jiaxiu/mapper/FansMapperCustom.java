package me.jiaxiu.mapper;

import me.jiaxiu.my.mapper.MyMapper;
import me.jiaxiu.pojo.Fans;
import me.jiaxiu.vo.FansVO;
import me.jiaxiu.vo.VlogerVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface FansMapperCustom extends MyMapper<Fans> {
    List<VlogerVO> queryMyFollows(@Param("paramMap") Map<String, Object> map);

    List<FansVO> queryMyFans(@Param("paramMap") Map<String, Object> map);
}