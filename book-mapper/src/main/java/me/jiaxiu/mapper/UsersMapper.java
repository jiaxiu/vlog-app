package me.jiaxiu.mapper;

import me.jiaxiu.my.mapper.MyMapper;
import me.jiaxiu.pojo.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersMapper extends MyMapper<Users> {
}