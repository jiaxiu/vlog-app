package me.jiaxiu.mapper;

import me.jiaxiu.my.mapper.MyMapper;
import me.jiaxiu.pojo.Vlog;
import org.springframework.stereotype.Repository;

@Repository
public interface VlogMapper extends MyMapper<Vlog> {
}