package me.jiaxiu.mapper;

import me.jiaxiu.my.mapper.MyMapper;
import me.jiaxiu.pojo.MyLikedVlog;
import org.springframework.stereotype.Repository;

@Repository
public interface MyLikedVlogMapper extends MyMapper<MyLikedVlog> {
}