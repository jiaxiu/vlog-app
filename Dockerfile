FROM openjdk:8-jre
MAINTAINER jiaxiu
COPY ./book-api/target/book-api-1.0-SNAPSHOT.jar /root/book-api-1.0-SNAPSHOT.jar
EXPOSE 8099
ENTRYPOINT ["java","-jar","/root/book-api-1.0-SNAPSHOT.jar"]