package me.jiaxiu.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.bo.CommentBO;
import me.jiaxiu.enums.MessageEnum;
import me.jiaxiu.pojo.Comment;
import me.jiaxiu.pojo.Vlog;
import me.jiaxiu.result.JSONResult;
import me.jiaxiu.service.CommentService;
import me.jiaxiu.service.MessageService;
import me.jiaxiu.service.VlogService;
import me.jiaxiu.utils.PagedGridResult;
import me.jiaxiu.vo.CommentVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Api(tags = "CommentController 评论业务功能接口")
@RequestMapping("comment")
@RestController
public class CommentController extends BaseInfoProperties {
    private final CommentService commentService;
    private final MessageService messageService;
    private final VlogService vlogService;

    public CommentController(CommentService commentService, MessageService messageService, VlogService vlogService) {
        this.commentService = commentService;
        this.messageService = messageService;
        this.vlogService = vlogService;
    }

    @PostMapping("create")
    public JSONResult create(@RequestBody @Valid CommentBO commentBO) {
        CommentVO commentVO = commentService.createComment(commentBO);

        return JSONResult.ok(commentVO);
    }

    @GetMapping("counts")
    public JSONResult counts(@RequestParam String vlogId) {
        String countStr = redisOperator.get(REDIS_VLOG_COMMENT_COUNTS + ":" + vlogId);
        return JSONResult.ok(Integer.valueOf(StringUtils.isNotBlank(countStr) ? countStr : "0"));
    }

    @GetMapping("list")
    public JSONResult list(
            @RequestParam String vlogId,
            @RequestParam(defaultValue = "") String userId,
            @RequestParam Integer page,
            @RequestParam Integer pageSize) {
        PagedGridResult pagedGridResult = commentService.queryVlogComments(vlogId, userId, page, pageSize);
        return JSONResult.ok(pagedGridResult);
    }

    @DeleteMapping("delete")
    public JSONResult delete(@RequestParam String commentUserId, @RequestParam String commentId, @RequestParam String vlogId) {
        commentService.deleteComment(vlogId, commentId, commentUserId);

        return JSONResult.ok();
    }


    @PostMapping("like")
    public JSONResult like(@RequestParam String commentId,
                           @RequestParam String userId) {

        // 故意犯错，bigkey
        redisOperator.incrementHash(REDIS_VLOG_COMMENT_LIKED_COUNTS, commentId, 1);
        redisOperator.setHashValue(REDIS_USER_LIKE_COMMENT, userId + ":" + commentId, "1");

        // 系统消息：点赞评论
        Comment comment = commentService.getComment(commentId);
        Vlog vlog = vlogService.getVlog(comment.getVlogId());
        Map<String, Object> msgContent = new HashMap<>();
        msgContent.put("vlogId", vlog.getId());
        msgContent.put("vlogCover", vlog.getCover());
        msgContent.put("commentId", commentId);

        messageService.createMsg(userId, comment.getCommentUserId(), MessageEnum.LIKE_COMMENT.type, msgContent);

        return JSONResult.ok();
    }

    @PostMapping("unlike")
    public JSONResult unlike(@RequestParam String commentId,
                             @RequestParam String userId) {

        redisOperator.decrementHash(REDIS_VLOG_COMMENT_LIKED_COUNTS, commentId, 1);
        redisOperator.hdel(REDIS_USER_LIKE_COMMENT, userId + ":" + commentId);

        return JSONResult.ok();
    }
}
