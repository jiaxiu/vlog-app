package me.jiaxiu.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.mo.MessageMO;
import me.jiaxiu.result.JSONResult;
import me.jiaxiu.service.MessageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("msg")
@Slf4j
@Api(tags = "MsgController 消息功能模块的接口")
public class MessageController extends BaseInfoProperties {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("list")
    public JSONResult list(@RequestParam String userId, @RequestParam Integer page, @RequestParam Integer pageSize) {
        // mongodb 从0分页，区别于数据库
        if (page == null) {
            page = COMMON_START_PAGE_ZERO;
        }
        if (pageSize == null) {
            pageSize = COMMON_PAGE_SIZE;
        }

        List<MessageMO> list = messageService.queryList(userId, page, pageSize);
        return JSONResult.ok(list);

    }
}
