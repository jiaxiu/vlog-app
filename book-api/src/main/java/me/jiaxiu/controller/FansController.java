package me.jiaxiu.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.pojo.Users;
import me.jiaxiu.result.JSONResult;
import me.jiaxiu.service.FansService;
import me.jiaxiu.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("fans")
@Slf4j
@Api(tags = "FansController 粉丝业务功能接口")
public class FansController extends BaseInfoProperties {
    private final FansService fansService;
    private final UserService userService;

    public FansController(FansService fansService, UserService userService) {
        this.fansService = fansService;
        this.userService = userService;
    }

    @PostMapping("follow")
    public JSONResult follow(@RequestParam String myId, @RequestParam String vlogerId) {
        // 参数不能为空
        if (StringUtils.isBlank(myId) || StringUtils.isBlank(vlogerId)) {
            return JSONResult.error();
        }
        // 不能关注自己
        if (myId.equalsIgnoreCase(vlogerId)) {
            return JSONResult.error();
        }
        // 判断是否存在两个ID对应用户
        Users myInfo = userService.getUser(myId);
        Users vlogerInfo = userService.getUser(vlogerId);

        if (vlogerInfo == null) {
            return JSONResult.error();
        }
        if (myInfo == null) {
            return JSONResult.error();
        }

        // 保存粉丝关系到数据库
        fansService.doFollow(myId, vlogerId);

        // vloger 粉丝数 +1，我都关注数 +1
        redisOperator.increment(REDIS_MY_FANS_COUNTS + ":" + vlogerId, 1);
        redisOperator.increment(REDIS_MY_FOLLOWS_COUNTS + ":" + myId, 1);
        // 我和博主的关联关系，依赖redis，不要存储数据库，避免db的性能瓶颈
        redisOperator.set(REDIS_FANS_AND_VLOGGER_RELATIONSHIP + ":" + myId + ":" + vlogerId, "1");

        return JSONResult.ok();
    }

    @PostMapping("cancel")
    public JSONResult cancel(@RequestParam String myId, @RequestParam String vlogerId) {
        fansService.doCancel(myId, vlogerId);

        // vloger 粉丝数 -1，我都关注数 -1
        redisOperator.decrement(REDIS_MY_FANS_COUNTS + ":" + vlogerId, 1);
        redisOperator.decrement(REDIS_MY_FOLLOWS_COUNTS + ":" + myId, 1);
        // 我和博主的关联关系，依赖redis，不要存储数据库，避免db的性能瓶颈
        redisOperator.del(REDIS_FANS_AND_VLOGGER_RELATIONSHIP + ":" + myId + ":" + vlogerId);

        return JSONResult.ok();
    }

    @GetMapping("cancel")
    public JSONResult queryDoIFollowVloger(@RequestParam String myId, @RequestParam String vlogerId) {
        return JSONResult.ok(fansService.queryDoIFollowVloger(myId, vlogerId));
    }

    @GetMapping("queryMyFollows")
    public JSONResult queryMyFollows(@RequestParam String myId, @RequestParam Integer page, @RequestParam Integer pageSize) {
        return JSONResult.ok(fansService.queryMyFollows(myId, page, pageSize));
    }

    @GetMapping("queryMyFans")
    public JSONResult queryMyFans(@RequestParam String myId, @RequestParam Integer page, @RequestParam Integer pageSize) {
        return JSONResult.ok(fansService.queryMyFans(myId, page, pageSize));
    }
}
