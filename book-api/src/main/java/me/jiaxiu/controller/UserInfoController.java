package me.jiaxiu.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.MinIOConfig;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.bo.UpdatedUserBO;
import me.jiaxiu.enums.FileTypeEnum;
import me.jiaxiu.enums.UserInfoModifyType;
import me.jiaxiu.pojo.Users;
import me.jiaxiu.result.JSONResult;
import me.jiaxiu.result.ResponseStatusEnum;
import me.jiaxiu.service.UserService;
import me.jiaxiu.utils.MinIOUtils;
import me.jiaxiu.vo.UsersVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("userInfo")
@Api(tags = "UserInfo 用户信息接口模块")
public class UserInfoController extends BaseInfoProperties {
    private final UserService userService;
    private final MinIOConfig minIOConfig;

    public UserInfoController(UserService userService, MinIOConfig minIOConfig) {
        this.userService = userService;
        this.minIOConfig = minIOConfig;
    }

    @GetMapping("query")
    public JSONResult query(@RequestParam String userId) {
        Users user = userService.getUser(userId);

        UsersVO usersVO = new UsersVO();
        BeanUtils.copyProperties(user, usersVO);

        // 我的关注博主总数量
        String myFollowsCountsStr = redisOperator.get(REDIS_MY_FOLLOWS_COUNTS + ":" + userId);
        // 我的粉丝总数
        String myFansCountsStr = redisOperator.get(REDIS_MY_FANS_COUNTS + ":" + userId);
        // 用户获赞总数，视频博主（点赞/喜欢）总和
//        String likedVlogCountsStr = redisOperator.get(REDIS_VLOG_BE_LIKED_COUNTS + ":" + userId);
        String likedVlogerCountsStr = redisOperator.get(REDIS_VLOGER_BE_LIKED_COUNTS + ":" + userId);

        Integer myFollowsCounts = 0;
        Integer myFansCounts = 0;
        Integer likedVlogCounts = 0;
        Integer likedVlogerCounts = 0;
        Integer totalLikeMeCounts = 0;

        if (StringUtils.isNotBlank(myFollowsCountsStr)) {
            myFollowsCounts = Integer.valueOf(myFollowsCountsStr);
        }
        if (StringUtils.isNotBlank(myFansCountsStr)) {
            myFansCounts = Integer.valueOf(myFansCountsStr);
        }
//        if (StringUtils.isNotBlank(likedVlogCountsStr)) {
//            likedVlogCounts = Integer.valueOf(likedVlogCountsStr);
//        }
        if (StringUtils.isNotBlank(likedVlogerCountsStr)) {
            likedVlogerCounts = Integer.valueOf(likedVlogerCountsStr);
        }
        totalLikeMeCounts = likedVlogCounts + likedVlogerCounts;

        usersVO.setMyFollowsCounts(myFollowsCounts);
        usersVO.setMyFansCounts(myFansCounts);
        usersVO.setTotalLikeMeCounts(totalLikeMeCounts);


        return JSONResult.ok(usersVO);
    }

    @PostMapping("modifyUserInfo")
    public JSONResult modifyUserInfo(@RequestBody UpdatedUserBO updatedUserBO, @RequestParam Integer type) {

        UserInfoModifyType.checkUserInfoTypeIsRight(type);

        Users newUserInfo = userService.updateUserInfo(updatedUserBO, type);

        return JSONResult.ok(newUserInfo);
    }

    @PostMapping("modifyImage")
    public JSONResult modifyImage(@RequestParam String userId,
                                  @RequestParam Integer type,
                                  MultipartFile file) throws Exception {
        if (type != FileTypeEnum.BGIMG.type && type != FileTypeEnum.FACE.type) {
            return JSONResult.errorCustom(ResponseStatusEnum.FILE_UPLOAD_FAILED);
        }
        // 取得文件名
        String filename = file.getOriginalFilename();

        // 存储至MinIO
        MinIOUtils.uploadFile(minIOConfig.getBucketName(), filename, file.getInputStream());

        // 拿到图像URL
        String imgUrl = minIOConfig.getFileHost() + "/"
                + minIOConfig.getBucketName() + "/"
                + filename;

        // 修改图片地址到数据库
        UpdatedUserBO updatedUserBO = new UpdatedUserBO();
        updatedUserBO.setId(userId);
        if (type == FileTypeEnum.BGIMG.type) {
            updatedUserBO.setBgImg(imgUrl);
        } else {
            updatedUserBO.setFace(imgUrl);
        }
        Users user = userService.updateUserInfo(updatedUserBO);

        return JSONResult.ok(user);
    }
}
