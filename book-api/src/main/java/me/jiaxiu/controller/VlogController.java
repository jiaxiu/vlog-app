package me.jiaxiu.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.bo.VlogBO;
import me.jiaxiu.enums.YesOrNo;
import me.jiaxiu.result.JSONResult;
import me.jiaxiu.service.VlogService;
import me.jiaxiu.utils.PagedGridResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("vlog")
@Slf4j
@Api(tags = "VlogController 短视频业务接口")
public class VlogController extends BaseInfoProperties {

    private final VlogService vlogService;

    public VlogController(VlogService vlogService) {
        this.vlogService = vlogService;
    }

    @PostMapping("publish")
    public JSONResult publish(@RequestBody VlogBO vlogBO) {
        vlogService.createVlog(vlogBO);

        return JSONResult.ok();
    }

    @GetMapping("indexList")
    public JSONResult indexList(
            @RequestParam(defaultValue = "") String userId,
            @RequestParam(defaultValue = "") String search,
            @RequestParam Integer page,
            @RequestParam Integer pageSize
    ) {
        if (page == null) {
            page = COMMON_START_PAGE;
        }
        if (pageSize == null) {
            pageSize = COMMON_PAGE_SIZE;
        }

        PagedGridResult pagedGridResult = vlogService.getIndexVlogList(userId, search, page, pageSize);
        return JSONResult.ok(pagedGridResult);
    }

    @GetMapping("detail")
    public JSONResult detail(@RequestParam(defaultValue = "") String userId, @RequestParam String vlogId) {
        return JSONResult.ok(vlogService.getVlogDetailById(userId, vlogId));
    }

    @PostMapping("changToPublic")
    public JSONResult changToPublic(@RequestParam String userId, @RequestParam String vlogId) {
        vlogService.changeToPrivateOrPublic(userId, vlogId, YesOrNo.NO.type);
        return JSONResult.ok();
    }

    @PostMapping("changeToPrivate")
    public JSONResult changToPrivate(@RequestParam String userId, @RequestParam String vlogId) {
        vlogService.changeToPrivateOrPublic(userId, vlogId, YesOrNo.YES.type);
        return JSONResult.ok();
    }

    @GetMapping("myPublicList")
    public JSONResult myPublicList(
            @RequestParam String userId,
            @RequestParam Integer page,
            @RequestParam Integer pageSize
    ) {
        if (page == null) {
            page = COMMON_START_PAGE;
        }
        if (pageSize == null) {
            pageSize = COMMON_PAGE_SIZE;
        }

        PagedGridResult vlogList = vlogService.queryMyVlogList(userId, page, pageSize, YesOrNo.NO.type);

        return JSONResult.ok(vlogList);
    }

    @GetMapping("myPrivateList")
    public JSONResult myPrivateList(
            @RequestParam String userId,
            @RequestParam Integer page,
            @RequestParam Integer pageSize
    ) {
        if (page == null) {
            page = COMMON_START_PAGE;
        }
        if (pageSize == null) {
            pageSize = COMMON_PAGE_SIZE;
        }

        PagedGridResult vlogList = vlogService.queryMyVlogList(userId, page, pageSize, YesOrNo.YES.type);

        return JSONResult.ok(vlogList);
    }

    @PostMapping("like")
    public JSONResult like(@RequestParam String userId, @RequestParam String vlogerId, @RequestParam String vlogId) {
        // 保存到数据库
        vlogService.userLikeVlog(userId, vlogId);

        // 视频发布者和视频的获赞数 +1
        redisOperator.increment(REDIS_VLOGER_BE_LIKED_COUNTS + ":" + vlogerId, 1);
        redisOperator.increment(REDIS_VLOG_BE_LIKED_COUNTS + ":" + vlogId, 1);

        // 在 redis 中保存
        redisOperator.set(REDIS_USER_LIKE_VLOG + ":" + userId + ":" + vlogId, "1");

        return JSONResult.ok();
    }

    @PostMapping("unlike")
    public JSONResult unlike(@RequestParam String userId, @RequestParam String vlogerId, @RequestParam String vlogId) {
        // 数据库中移除
        vlogService.userUnlikeVlog(userId, vlogId);

        // 视频发布者和视频的获赞数 -1
        redisOperator.decrement(REDIS_VLOGER_BE_LIKED_COUNTS + ":" + vlogerId, 1);
        redisOperator.decrement(REDIS_VLOG_BE_LIKED_COUNTS + ":" + vlogId, 1);

        // 在 redis 中移除
        redisOperator.del(REDIS_USER_LIKE_VLOG + ":" + userId + ":" + vlogId);

        return JSONResult.ok();
    }

    @PostMapping("totalLikedCounts")
    public JSONResult totalLikedCounts(@RequestParam String vlogId) {
        return JSONResult.ok(vlogService.getVlogBeLikedCounts(vlogId));
    }

    @GetMapping("myLikedVlogList")
    public JSONResult myLikedVlogList(@RequestParam String userId, @RequestParam Integer page, @RequestParam Integer pageSize) {
        if (page == null) {
            page = COMMON_START_PAGE;
        }
        if (pageSize == null) {
            pageSize = COMMON_PAGE_SIZE;
        }
        PagedGridResult myLikedVlogList = vlogService.getMyLikedVlogList(userId, page, pageSize);

        return JSONResult.ok(myLikedVlogList);
    }

    @GetMapping("followList")
    public JSONResult followList(@RequestParam String myId, @RequestParam Integer page, @RequestParam Integer pageSize) {
        if (page == null) {
            page = COMMON_START_PAGE;
        }
        if (pageSize == null) {
            pageSize = COMMON_PAGE_SIZE;
        }

        PagedGridResult myFollowVlogList = vlogService.getMyFollowVlogList(myId, page, pageSize);

        return JSONResult.ok(myFollowVlogList);
    }

    @GetMapping("friendList")
    public JSONResult friendList(@RequestParam String myId, @RequestParam Integer page, @RequestParam Integer pageSize) {
        if (page == null) {
            page = COMMON_START_PAGE;
        }
        if (pageSize == null) {
            pageSize = COMMON_PAGE_SIZE;
        }

        PagedGridResult myFollowVlogList = vlogService.getMyFriendVlogList(myId, page, pageSize);

        return JSONResult.ok(myFollowVlogList);
    }
}
