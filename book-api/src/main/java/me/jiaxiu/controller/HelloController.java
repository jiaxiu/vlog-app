package me.jiaxiu.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.result.JSONResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Api(tags = "Hello 测试的接口")
public class HelloController {
    @GetMapping("hello")
    @ApiOperation("hello - 这是一个hello的测试路由")
    public Object hello() {
        log.debug("hello() method running");
        log.info("hello() method running");
        log.warn("hello() method running");
        log.error("hello() method running");
        return JSONResult.ok("hello spring boot");
    }
}
