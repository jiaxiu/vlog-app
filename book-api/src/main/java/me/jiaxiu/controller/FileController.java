package me.jiaxiu.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.MinIOConfig;
import me.jiaxiu.result.JSONResult;
import me.jiaxiu.utils.MinIOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@Slf4j
@Api(tags = "FileController 文件上传测试的接口")
public class FileController {

    private final MinIOConfig minIOConfig;

    public FileController(MinIOConfig minIOConfig) {
        this.minIOConfig = minIOConfig;
    }

    @PostMapping("upload")
    public JSONResult upload(MultipartFile file) throws Exception {
        // 取得文件名
        String filename = file.getOriginalFilename();

        // 存储至MinIO
        MinIOUtils.uploadFile(minIOConfig.getBucketName(), filename, file.getInputStream());

        // 拿到图像URL
        String imgUrl = minIOConfig.getFileHost() + "/"
                + minIOConfig.getBucketName() + "/"
                + filename;

        return JSONResult.ok(imgUrl);
    }
}
