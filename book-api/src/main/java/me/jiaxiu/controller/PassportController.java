package me.jiaxiu.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.bo.RegistLoginBO;
import me.jiaxiu.pojo.Users;
import me.jiaxiu.result.JSONResult;
import me.jiaxiu.result.ResponseStatusEnum;
import me.jiaxiu.service.UserService;
import me.jiaxiu.utils.IPUtil;
import me.jiaxiu.utils.SMSUtils;
import me.jiaxiu.vo.UsersVO;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("passport")
@RestController
@Slf4j
@Api(tags = "Passport 通信证接口模块")
public class PassportController extends BaseInfoProperties {
    private final SMSUtils smsUtils;
    private final UserService userService;

    public PassportController(SMSUtils smsUtils, UserService userService) {
        this.smsUtils = smsUtils;
        this.userService = userService;
    }

    @PostMapping("getSMSCode")
    public JSONResult getSMSCode(@RequestParam String mobile, HttpServletRequest request) {
        log.info("手机号码: {} ", mobile);
        if (StringUtils.isBlank(mobile)) {
            return JSONResult.ok();
        }

        // 获取用户IP， 同IP限制 60s 内不可再次获取验证码
        String requestIp = IPUtil.getRequestIp(request);
        // : 2022/6/25 限制用户在 60s 内只能获取一次验证码
        redisOperator.setnx60s(MOBILE_SMS_CODE + ":" + requestIp, requestIp);

        // 生成六位数字短信验证码
        String code = String.valueOf(RandomUtils.nextInt(100000, 999999));
        log.info("生成短信验证码：" + code);

        //  2022/6/25 验证码放入 redis 中，用于后续验证
        redisOperator.set(MOBILE_SMS_CODE + ":" + mobile, code, 5 * 60);


        // 发送短信验证码
//        smsUtils.sendSMS(mobile, code);
        log.info("向手机号 {} 发送验证码： {}", mobile, code);

        return JSONResult.ok();
    }

    @PostMapping("login")
    public JSONResult login(@Valid @RequestBody RegistLoginBO registLoginBO) {
        String mobile = registLoginBO.getMobile();
        String code = registLoginBO.getSmsCode();

        // 1. 从redis中获得验证码进行校验是否匹配
        String redisCode = redisOperator.get(MOBILE_SMS_CODE + ":" + mobile);
        if (StringUtils.isBlank(redisCode) || !redisCode.equalsIgnoreCase(code)) {
            return JSONResult.errorCustom(ResponseStatusEnum.SMS_CODE_ERROR);
        }

        // 2. 查询数据库，判断用户是否存在
        Users user = userService.queryMobileIsExist(mobile);
        if (user == null) {
            // 2.1 如果用户为空，表示没有注册过，则为null，需要注册信息入库
            user = userService.createUser(mobile);
        }

        // 3. 如果不为空，可以继续下方业务，可以保存用户会话信息
        String uToken = UUID.randomUUID().toString();
        redisOperator.set(REDIS_USER_TOKEN + ":" + user.getId(), uToken);

        // 4. 用户登录注册成功以后，删除redis中的短信验证码
        redisOperator.del(MOBILE_SMS_CODE + ":" + mobile);

        // 5. 返回用户信息，包含token令牌
        UsersVO usersVO = new UsersVO();
        BeanUtils.copyProperties(user, usersVO);
        usersVO.setUserToken(uToken);

        return JSONResult.ok(usersVO);
    }


    @PostMapping("logout")
    public JSONResult logout(@RequestParam String userId) {

        // 后端只需要清除用户的token信息即可，前端也需要清除，清除本地app中的用户信息和token会话信息
        redisOperator.del(REDIS_USER_TOKEN + ":" + userId);

        return JSONResult.ok();
    }
}
