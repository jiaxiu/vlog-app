package me.jiaxiu.interceptor;

import lombok.extern.slf4j.Slf4j;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.result.ResponseStatusEnum;
import me.jiaxiu.utils.IPUtil;
import me.jiaxiu.utils.MyExceptionUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class PassportInterceptor extends BaseInfoProperties implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获得用户 IP
        String requestIp = IPUtil.getRequestIp(request);

        // 得到是否存在的判断
        boolean keyIsExist = redisOperator.keyIsExist(MOBILE_SMS_CODE + ":" + requestIp);

        if (keyIsExist) {
            MyExceptionUtils.display(ResponseStatusEnum.SMS_NEED_WAIT_ERROR);
            log.info("验证码请求过于频繁");
            return false;
        }

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
