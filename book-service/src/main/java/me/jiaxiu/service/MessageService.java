package me.jiaxiu.service;

import me.jiaxiu.mo.MessageMO;

import java.util.List;
import java.util.Map;

public interface MessageService {
    /**
     * 创建消息
     */
    void createMsg(String fromUserId, String toUserId, Integer type, Map<String, Object> msgContent);

    List<MessageMO> queryList(String toUserId, Integer page, Integer pageSize);
}
