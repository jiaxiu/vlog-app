package me.jiaxiu.service;

import me.jiaxiu.bo.CommentBO;
import me.jiaxiu.pojo.Comment;
import me.jiaxiu.utils.PagedGridResult;
import me.jiaxiu.vo.CommentVO;

public interface CommentService {
    /**
     * 发表评论
     */
    CommentVO createComment(CommentBO commentBO);

    /**
     * 查询评论列表
     */
    PagedGridResult queryVlogComments(String vlogId, String userId, Integer page, Integer pageSize);

    /**
     * 删除评论
     *
     * @param vlogId        评论所属的视频ID
     * @param commentId     评论ID
     * @param commentUserId 发表评论的用户ID
     */
    void deleteComment(String vlogId, String commentId, String commentUserId);

    Comment getComment(String commentId);
}
