package me.jiaxiu.service;

import me.jiaxiu.bo.UpdatedUserBO;
import me.jiaxiu.pojo.Users;

public interface UserService {
    /**
     * 判断用户是否存在，如果存在则返回用户信息
     *
     * @param mobile 用户手机号码
     * @return 当用户存在时返回用户信息，否则为 null
     */
    Users queryMobileIsExist(String mobile);

    /**
     * 创建用户信息并且返回用户对象
     *
     * @param mobile 用户手机号
     * @return 用户对象
     */
    Users createUser(String mobile);

    /**
     * 根据用户id查询用户信息
     *
     * @param userId 用户id
     * @return 用户信息
     */
    Users getUser(String userId);

    Users updateUserInfo(UpdatedUserBO updatedUserBO);

    Users updateUserInfo(UpdatedUserBO updatedUserBO, Integer type);
}
