package me.jiaxiu.service;

import me.jiaxiu.utils.PagedGridResult;

public interface FansService {
    /**
     * 关注
     *
     * @param myId     当前用户ID
     * @param vlogerId 要关注的用户ID
     */
    void doFollow(String myId, String vlogerId);

    /**
     * 取关
     *
     * @param myId     当前用户ID
     * @param vlogerId 要取关的用户ID
     */
    void doCancel(String myId, String vlogerId);

    /**
     * @param myId     当前用户ID
     * @param vlogerId 目标用户ID
     * @return 已关注为 true，未关注为 false
     */
    boolean queryDoIFollowVloger(String myId, String vlogerId);

    PagedGridResult queryMyFollows(String myId, Integer page, Integer pageSize);

    PagedGridResult queryMyFans(String myId, Integer page, Integer pageSize);
}
