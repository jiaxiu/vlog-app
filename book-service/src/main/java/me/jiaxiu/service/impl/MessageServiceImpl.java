package me.jiaxiu.service.impl;

import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.enums.MessageEnum;
import me.jiaxiu.mo.MessageMO;
import me.jiaxiu.pojo.Users;
import me.jiaxiu.repository.MessageRepository;
import me.jiaxiu.service.MessageService;
import me.jiaxiu.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MessageServiceImpl extends BaseInfoProperties implements MessageService {
    private final MessageRepository messageRepository;
    private final UserService userService;

    public MessageServiceImpl(MessageRepository messageRepository, UserService userService) {
        this.messageRepository = messageRepository;
        this.userService = userService;
    }

    @Override
    public void createMsg(String fromUserId, String toUserId, Integer type, Map<String, Object> msgContent) {
        Users fromUser = userService.getUser(fromUserId);

        MessageMO messageMO = new MessageMO();

        messageMO.setFromUserId(fromUserId);
        messageMO.setFromNickname(fromUser.getNickname());
        messageMO.setFromFace(fromUser.getFace());

        messageMO.setToUserId(toUserId);

        messageMO.setMsgType(type);
        if (msgContent != null) {
            messageMO.setMsgContent(msgContent);
        }

        messageMO.setCreateTime(new Date());

        messageRepository.save(messageMO);
    }

    @Override
    public List<MessageMO> queryList(String toUserId, Integer page, Integer pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createTime");
        List<MessageMO> list = messageRepository.findAllByToUserIdOrderByCreateTimeDesc(toUserId, pageable);
        list.stream()
                .filter(msg -> msg.getMsgType() != null && msg.getMsgType().equals(MessageEnum.FOLLOW_YOU.type)) // 如果类型是关注消息，则需要查询我之前有没有关注过他，用于在前端标记“互粉”“互关”
                .peek(msg -> msg.setMsgContent(msg.getMsgContent() == null ? new HashMap<>() : msg.getMsgContent()))
                .forEach(msg -> {
                    String relationship = redisOperator.get(REDIS_FANS_AND_VLOGGER_RELATIONSHIP + ":" + msg.getToUserId() + ":" + msg.getFromUserId());
                    msg.getMsgContent().put("isFriend", StringUtils.isNotBlank(relationship) && relationship.equalsIgnoreCase("1"));
                });
        return list;
    }
}
