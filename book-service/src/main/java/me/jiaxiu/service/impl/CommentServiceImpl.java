package me.jiaxiu.service.impl;

import com.github.pagehelper.PageHelper;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.bo.CommentBO;
import me.jiaxiu.enums.MessageEnum;
import me.jiaxiu.enums.YesOrNo;
import me.jiaxiu.mapper.CommentMapper;
import me.jiaxiu.mapper.CommentMapperCustom;
import me.jiaxiu.pojo.Comment;
import me.jiaxiu.pojo.Vlog;
import me.jiaxiu.service.CommentService;
import me.jiaxiu.service.MessageService;
import me.jiaxiu.service.VlogService;
import me.jiaxiu.utils.PagedGridResult;
import me.jiaxiu.vo.CommentVO;
import org.apache.commons.lang3.StringUtils;
import org.n3r.idworker.Sid;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommentServiceImpl extends BaseInfoProperties implements CommentService {

    private final CommentMapper commentMapper;
    private final CommentMapperCustom commentMapperCustom;
    private final Sid sid;
    private final MessageService messageService;
    private final VlogService vlogService;

    public CommentServiceImpl(CommentMapper commentMapper, CommentMapperCustom commentMapperCustom, Sid sid, MessageService messageService, VlogService vlogService) {
        this.commentMapper = commentMapper;
        this.commentMapperCustom = commentMapperCustom;
        this.sid = sid;
        this.messageService = messageService;
        this.vlogService = vlogService;
    }

    @Override
    public CommentVO createComment(CommentBO commentBO) {
        Comment comment = new Comment();

        String cId = sid.nextShort();
        BeanUtils.copyProperties(commentBO, comment);
        comment.setId(cId);
        comment.setLikeCounts(0);
        comment.setCreateTime(new Date());

        commentMapper.insert(comment);

        // redis操作放在service中，评论总数的累加
        redisOperator.increment(REDIS_VLOG_COMMENT_COUNTS + ":" + commentBO.getVlogId(), 1);

        // 留言后的最新评论需要返回给前端进行展示
        CommentVO commentVO = new CommentVO();
        BeanUtils.copyProperties(comment, commentVO);

        // 系统消息：评论/回复
        Vlog vlog = vlogService.getVlog(comment.getVlogId());
        Map<String, Object> msgContent = new HashMap<>();
        msgContent.put("vlogId", vlog.getId());
        msgContent.put("vlogCover", vlog.getCover());
        msgContent.put("commentId", comment.getId());
        msgContent.put("commentContent", comment.getContent());
        Integer type =
                StringUtils.isBlank(comment.getFatherCommentId()) || comment.getFatherCommentId().equalsIgnoreCase("0")
                        ? MessageEnum.COMMENT_VLOG.type
                        : MessageEnum.REPLY_YOU.type;
        messageService.createMsg(commentBO.getCommentUserId(), commentBO.getVlogerId(), type, msgContent);


        return commentVO;
    }

    @Override
    public PagedGridResult queryVlogComments(String vlogId, String userId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);

        HashMap<String, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(vlogId)) {
            map.put("vlogId", vlogId);
        }

        List<CommentVO> commentList = commentMapperCustom.getCommentList(map);

        commentList.forEach(c -> {
            // 获取评论的点赞总数
            String countStr = redisOperator.getHashValue(REDIS_VLOG_COMMENT_LIKED_COUNTS, c.getCommentId());
            c.setLikeCounts(Integer.valueOf(StringUtils.isNotBlank(countStr) ? countStr : "0"));

            // 判断当前用户是否点赞过该评论
            String doILike = redisOperator.hget(REDIS_USER_LIKE_COMMENT, userId + ":" + c.getCommentId());
            c.setIsLike(StringUtils.isNotBlank(doILike) && doILike.equalsIgnoreCase("1") ? YesOrNo.YES.type : YesOrNo.NO.type);
        });

        return setterPagedGrid(commentList, page);
    }

    @Override
    public void deleteComment(String vlogId, String commentId, String commentUserId) {
        Comment pendingComment = new Comment();
        pendingComment.setId(commentId);
        pendingComment.setCommentUserId(commentUserId);

        commentMapper.delete(pendingComment);
        redisOperator.decrement(REDIS_VLOG_COMMENT_COUNTS + ":" + vlogId, 1);
    }

    @Override
    public Comment getComment(String commentId) {
        return commentMapper.selectByPrimaryKey(commentId);
    }
}
