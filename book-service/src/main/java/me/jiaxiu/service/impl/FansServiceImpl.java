package me.jiaxiu.service.impl;

import com.github.pagehelper.PageHelper;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.enums.MessageEnum;
import me.jiaxiu.enums.YesOrNo;
import me.jiaxiu.mapper.FansMapper;
import me.jiaxiu.mapper.FansMapperCustom;
import me.jiaxiu.pojo.Fans;
import me.jiaxiu.service.FansService;
import me.jiaxiu.service.MessageService;
import me.jiaxiu.utils.PagedGridResult;
import me.jiaxiu.vo.FansVO;
import me.jiaxiu.vo.VlogerVO;
import org.apache.commons.lang3.StringUtils;
import org.n3r.idworker.Sid;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FansServiceImpl extends BaseInfoProperties implements FansService {

    private final FansMapper fansMapper;
    private final FansMapperCustom fansMapperCustom;
    private final Sid sid;
    private final MessageService messageService;

    public FansServiceImpl(FansMapper fansMapper, FansMapperCustom fansMapperCustom, Sid sid, MessageService messageService) {
        this.fansMapper = fansMapper;
        this.fansMapperCustom = fansMapperCustom;
        this.sid = sid;
        this.messageService = messageService;
    }

    @Transactional
    @Override
    public void doFollow(String myId, String vlogerId) {
        String fId = sid.nextShort();

        Fans fans = new Fans();
        fans.setId(fId);
        fans.setFanId(myId);
        fans.setVlogerId(vlogerId);

        // 判断是否互关
        Fans vloger = queryFansRelationship(vlogerId, myId);
        if (vloger == null) {
            fans.setIsFanFriendOfMine(YesOrNo.NO.type);
        } else {
            fans.setIsFanFriendOfMine(YesOrNo.YES.type);
            vloger.setIsFanFriendOfMine(YesOrNo.YES.type);
            fansMapper.updateByPrimaryKeySelective(vloger);
        }

        fansMapper.insert(fans);

        // 系统消息：关注
        messageService.createMsg(myId, vlogerId, MessageEnum.FOLLOW_YOU.type, null);
    }

    @Transactional
    @Override
    public void doCancel(String myId, String vlogerId) {
        // 判断是否需要取消互关
        Fans fan = queryFansRelationship(myId, vlogerId);
        if (fan != null && fan.getIsFanFriendOfMine().equals(YesOrNo.YES.type)) {
            Fans pendingFan = queryFansRelationship(vlogerId, myId);
            pendingFan.setIsFanFriendOfMine(YesOrNo.NO.type);
            fansMapper.updateByPrimaryKeySelective(pendingFan);
        }

        // 删除自己的关注关联表记录
        fansMapper.delete(fan);
    }

    @Override
    public boolean queryDoIFollowVloger(String myId, String vlogerId) {
        return queryFansRelationship(myId, vlogerId) != null;
    }

    @Override
    public PagedGridResult queryMyFollows(String myId, Integer page, Integer pageSize) {
        Map<String, Object> map = new HashMap<>();
        map.put("myId", myId);

        PageHelper.startPage(page, pageSize);
        List<VlogerVO> myFollows = fansMapperCustom.queryMyFollows(map);

        return setterPagedGrid(myFollows, page);
    }

    @Override
    public PagedGridResult queryMyFans(String myId, Integer page, Integer pageSize) {
        Map<String, Object> map = new HashMap<>();
        map.put("myId", myId);

        PageHelper.startPage(page, pageSize);
        List<FansVO> myFans = fansMapperCustom.queryMyFans(map);

        /**
         * <判断粉丝是否是我的朋友（互粉互关）>
         * 普通做法：
         * 多表关联+嵌套关联查询，这样会违反多表关联的规范，不可取，高并发下回出现性能问题
         *
         * 常规做法：
         * 1. 避免过多的表关联查询，先查询我的粉丝列表，获得fansList
         * 2. 判断粉丝关注我，并且我也关注粉丝 -> 循环fansList，获得每一个粉丝，再去数据库查询我是否关注他
         * 3. 如果我也关注他（粉丝），说明，我俩互为朋友关系（互关互粉），则标记flag为true，否则false
         *
         * 当前做法：
         * 1. 关注/取关的时候，关联关系保存在redis中，不要依赖数据库
         * 2. 数据库查询后，直接循环查询redis，避免第二次循环查询数据库的尴尬局面
         */
        myFans.forEach(fan -> {
            String relationship = redisOperator.get(REDIS_FANS_AND_VLOGGER_RELATIONSHIP + ":" + myId + ":" + fan.getFanId());
            if (StringUtils.isNotBlank(relationship) && relationship.equalsIgnoreCase("1")) {
                fan.setFriend(true);
            }
        });

        return setterPagedGrid(myFans, page);
    }

    private Fans queryFansRelationship(String fanId, String vlogerId) {
        Example example = new Example(Fans.class);
        example.createCriteria()
                .andEqualTo("vlogerId", vlogerId)
                .andEqualTo("fanId", fanId);

        List<Fans> list = fansMapper.selectByExample(example);

        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }
}
