package me.jiaxiu.service.impl;

import com.github.pagehelper.PageHelper;
import me.jiaxiu.base.BaseInfoProperties;
import me.jiaxiu.bo.VlogBO;
import me.jiaxiu.enums.MessageEnum;
import me.jiaxiu.enums.YesOrNo;
import me.jiaxiu.mapper.MyLikedVlogMapper;
import me.jiaxiu.mapper.VlogMapper;
import me.jiaxiu.mapper.VlogMapperCustom;
import me.jiaxiu.pojo.MyLikedVlog;
import me.jiaxiu.pojo.Vlog;
import me.jiaxiu.service.FansService;
import me.jiaxiu.service.MessageService;
import me.jiaxiu.service.VlogService;
import me.jiaxiu.utils.PagedGridResult;
import me.jiaxiu.vo.IndexVlogVO;
import org.apache.commons.lang3.StringUtils;
import org.n3r.idworker.Sid;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VlogServiceImpl extends BaseInfoProperties implements VlogService {

    private final VlogMapper vlogMapper;
    private final VlogMapperCustom vlogMapperCustom;
    private final MyLikedVlogMapper myLikedVlogMapper;
    private final Sid sid;

    private final MessageService messageService;

    private final FansService fansService;

    public VlogServiceImpl(VlogMapper vlogMapper, VlogMapperCustom vlogMapperCustom, MyLikedVlogMapper myLikedVlogMapper, Sid sid, MessageService messageService, FansService fansService) {
        this.vlogMapper = vlogMapper;
        this.vlogMapperCustom = vlogMapperCustom;
        this.myLikedVlogMapper = myLikedVlogMapper;
        this.sid = sid;
        this.messageService = messageService;
        this.fansService = fansService;
    }

    @Transactional
    @Override
    public void createVlog(VlogBO vlogBO) {
        Vlog vlog = new Vlog();
        BeanUtils.copyProperties(vlogBO, vlog);

        String vid = sid.nextShort();

        vlog.setId(vid);
        vlog.setLikeCounts(0);
        vlog.setCommentsCounts(0);
        vlog.setIsPrivate(YesOrNo.NO.type);
        Date curr = new Date();
        vlog.setCreatedTime(curr);
        vlog.setUpdatedTime(curr);

        vlogMapper.insert(vlog);
    }

    @Override
    public PagedGridResult getIndexVlogList(String userId, String search, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);

        HashMap<String, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(search)) {
            map.put("search", search);
        }
        List<IndexVlogVO> vlogList = vlogMapperCustom.getIndexVlogList(map);

        vlogList.forEach(vlog -> setterVo(userId, vlog));

        return setterPagedGrid(vlogList, page);
    }

    @Override
    public Integer getVlogBeLikedCounts(String vlogId) {
        String countsStr = redisOperator.get(REDIS_VLOG_BE_LIKED_COUNTS + ":" + vlogId);
        return Integer.valueOf(StringUtils.isBlank(countsStr) ? "0" : countsStr);
    }

    @Override
    public PagedGridResult getMyLikedVlogList(String userId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);

        HashMap<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        List<IndexVlogVO> myLikedVlogList = vlogMapperCustom.getMyLikedVlogList(map);

        return setterPagedGrid(myLikedVlogList, page);
    }

    @Override
    public PagedGridResult getMyFollowVlogList(String myId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);

        HashMap<String, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(myId)) {
            map.put("myId", myId);
        }
        List<IndexVlogVO> vlogList = vlogMapperCustom.getMyFollowVlogList(map);

        vlogList.forEach(vlog -> setterVo(myId, vlog));

        return setterPagedGrid(vlogList, page);
    }

    @Override
    public PagedGridResult getMyFriendVlogList(String myId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);

        HashMap<String, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(myId)) {
            map.put("myId", myId);
        }

        List<IndexVlogVO> vlogList = vlogMapperCustom.getMyFriendVlogList(map);

        vlogList.forEach(vlog -> setterVo(myId, vlog));

        return setterPagedGrid(vlogList, page);
    }

    @Override
    public IndexVlogVO getVlogDetailById(String userId, String vlogId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("vlogId", vlogId);

        List<IndexVlogVO> list = vlogMapperCustom.getVlogDetailById(map);
        if (list != null && !list.isEmpty()) {
            return setterVo(userId, list.get(0));
        }

        return null;
    }

    @Transactional
    @Override
    public void changeToPrivateOrPublic(String userId, String vlogId, Integer yesOrNo) {
        Example example = new Example(Vlog.class);
        example.createCriteria()
                .andEqualTo("id", vlogId)
                .andEqualTo("vlogerId", userId);

        Vlog pendingVlog = new Vlog();
        pendingVlog.setIsPrivate(yesOrNo);

        vlogMapper.updateByExampleSelective(pendingVlog, example);
    }

    @Override
    public PagedGridResult queryMyVlogList(String userId, Integer page, Integer pageSize, Integer yesOrNo) {
        Example example = new Example(Vlog.class);
        example.createCriteria()
                .andEqualTo("vlogerId", userId)
                .andEqualTo("isPrivate", yesOrNo);

        PageHelper.startPage(page, pageSize);
        List<Vlog> vlogs = vlogMapper.selectByExample(example);

        return setterPagedGrid(vlogs, page);
    }

    @Transactional
    @Override
    public void userLikeVlog(String userId, String vlogId) {
        String id = sid.nextShort();
        MyLikedVlog myLikedVlog = new MyLikedVlog();
        myLikedVlog.setId(id);
        myLikedVlog.setUserId(userId);
        myLikedVlog.setVlogId(vlogId);

        myLikedVlogMapper.insert(myLikedVlog);

        // 系统消息：点赞视频
        Vlog vlog = this.getVlog(vlogId);
        Map<String, Object> msgContent = new HashMap<>();
        msgContent.put("vlogId", vlogId);
        msgContent.put("vlogCover", vlog.getCover());
        messageService.createMsg(userId, vlog.getVlogerId(), MessageEnum.LIKE_VLOG.type, msgContent);
    }

    @Override
    public Vlog getVlog(String vlogId) {
        return vlogMapper.selectByPrimaryKey(vlogId);
    }

    @Transactional
    @Override
    public void userUnlikeVlog(String userId, String vlogId) {
        MyLikedVlog myLikedVlog = new MyLikedVlog();
        myLikedVlog.setUserId(userId);
        myLikedVlog.setVlogId(vlogId);

        myLikedVlogMapper.delete(myLikedVlog);
    }

    private boolean isLikedVlog(String userId, String vlogId) {
        String liked = redisOperator.get(REDIS_USER_LIKE_VLOG + ":" + userId + ":" + vlogId);
        return StringUtils.isNotBlank(liked) && liked.equalsIgnoreCase("1");
    }

    private IndexVlogVO setterVo(String userId, IndexVlogVO vlog) {
        // 当前用户是否关注博主
        vlog.setDoIFollowVloger(fansService.queryDoIFollowVloger(userId, vlog.getVlogerId()));
        // 当前用户是否点赞过视频
        vlog.setDoILikeThisVlog(isLikedVlog(userId, vlog.getVlogId()));
        // 获取视频被点赞过的总数
        vlog.setLikeCounts(getVlogBeLikedCounts(vlog.getVlogId()));

        return vlog;
    }
}
