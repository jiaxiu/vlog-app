package me.jiaxiu.service;

import me.jiaxiu.bo.VlogBO;
import me.jiaxiu.pojo.Vlog;
import me.jiaxiu.utils.PagedGridResult;
import me.jiaxiu.vo.IndexVlogVO;

public interface VlogService {
    /**
     * 新增 Vlog
     */
    void createVlog(VlogBO vlogBO);

    PagedGridResult getIndexVlogList(String userId, String search, Integer page, Integer pageSize);

    /**
     * 根据主键查询 vlog
     */
    IndexVlogVO getVlogDetailById(String userId, String vlogId);

    Vlog getVlog(String vlogId);

    void changeToPrivateOrPublic(String userId, String vlogId, Integer yesOrNo);

    /**
     * 查询用户的公开/私密视频列表
     */
    PagedGridResult queryMyVlogList(String userId, Integer page, Integer pageSize, Integer yesOrNo);

    /**
     * 用户喜欢/点赞视频
     */
    void userLikeVlog(String userId, String vlogId);

    void userUnlikeVlog(String userId, String vlogId);

    /**
     * 获取视频被点赞的总数
     *
     * @param vlogId 视频ID
     * @return 点赞总数
     */
    Integer getVlogBeLikedCounts(String vlogId);

    /**
     * 查询用户点赞过的视频
     *
     * @param userId   用户ID
     * @param page     页码
     * @param pageSize 页面大小
     * @return 封装的视频列表
     */
    PagedGridResult getMyLikedVlogList(String userId, Integer page, Integer pageSize);

    /**
     * 查询用户关注博主的视频
     *
     * @param myId     用户ID
     * @param page     页码
     * @param pageSize 页面大小
     * @return 封装的视频列表
     */
    PagedGridResult getMyFollowVlogList(String myId, Integer page, Integer pageSize);

    /**
     * 查询互关博主的视频
     *
     * @param myId     用户ID
     * @param page     页码
     * @param pageSize 页面大小
     * @return 封装的视频列表
     */
    PagedGridResult getMyFriendVlogList(String myId, Integer page, Integer pageSize);
}
