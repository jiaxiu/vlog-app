package me.jiaxiu.utils;


import me.jiaxiu.exceptions.MyCustomException;
import me.jiaxiu.result.ResponseStatusEnum;

/**
 * 优雅的处理异常，统一封装
 */
public class MyExceptionUtils  {

    public static void display(ResponseStatusEnum responseStatusEnum)  {
        throw new MyCustomException(responseStatusEnum);
    }

}
